const express = require("express");
const hbs = require("express-handlebars");
const homeRouter = require("./src/routes/homeRouter");
const searchResultsRouter = require("./src/routes/searchResultsRouter");
const articleRouter = require("./src/routes/articleRouter");
const initApp = require("./middleware");

const app = express();
const port = 3002;
app.set("view engine", "hbs");

initApp(app);

app.engine(
  "hbs",
  hbs({
    extname: "hbs",
    defaultView: "default",
    layoutsDir: __dirname + "/views/layouts",
    partialsDir: __dirname + "/views/partials/"
  })
);

app.get("/", homeRouter);

app.post("/search-results", searchResultsRouter);

app.get("/article/:articleId", articleRouter);

app.listen(process.env.PORT || port, () =>
  console.log(`FT app listening on port ${port}!`)
);

module.exports = app;
