const { MAX_ITEMS } = require("../constants/paging");

const FT_REQUEST_PAYLOAD = {
  queryString: "",
  resultContext: {
    aspects: [
      "title",
      "lifecycle",
      "images",
      "location",
      "summary",
      "editorial"
    ],
    maxResults: MAX_ITEMS.toString(),
    offset: "0"
  }
};

module.exports = { FT_REQUEST_PAYLOAD };