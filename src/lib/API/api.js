const { Axios } = require("axios-observable");

// Interceptors
const onRequestSuccess = config => {
  config.headers[
    "X-Api-Key"
  ] = `59cbaf20e3e06d3565778e7bb3124288b28b478aaa06ba6734ba02b5`;
  config.headers["Content-Type"] = "application/json";
  return config;
};

const onResponseSuccess = response => {
  return response.data;
};

const onResponseError = err => {
  return Promise.reject(err);
};

// Endpoint Configuration
const endpoint = "http://api.ft.com/content/";

// API Configuration
const APIR = Axios.create({
  baseURL: endpoint
});

APIR.defaults.headers.post["Content-Type"] = "application/json;charset=utf-8";
APIR.defaults.headers.post["Access-Control-Allow-Origin"] = "*";

APIR.interceptors.request.use(onRequestSuccess);
APIR.interceptors.response.use(onResponseSuccess, onResponseError);

module.exports = { APIR, endpoint };
