const transformArticle = article => ({
  ...article,
  image: (article.images[0] || {} ).url,
  title: article.title.title,
  location: article.location.uri,
  category: article.editorial.byline
});

const transformPaging = (paging, totalCount, searchTerm) => ({
  showPrev: paging.offset !== 0,
  prevOffset: paging.offset - paging.maxResults,
  showNext: paging.offset + paging.maxResults < totalCount,
  nextOffset: paging.offset + paging.maxResults,
  page: Math.round(paging.offset / paging.maxResults) + 1,
  offset: paging.offset,
  maxitems: paging.maxResults,
  totalCount: Math.round(totalCount / paging.maxResults),
  searchTerm
});

module.exports = { transformArticle, transformPaging };
