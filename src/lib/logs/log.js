const logger = log => {
  console.log(log);
};

const errorLogger = error => {
  console.error(error);
};

module.exports = { logger, errorLogger };
