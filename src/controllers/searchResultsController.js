const { APIR } = require("../lib/API/api");
const logger = require("../lib/logs/log");
const { map, tap } = require("rxjs/operators");
const { FT_REQUEST_PAYLOAD } = require("../lib/payloads/ft.payload");
const {
  transformArticle,
  transformPaging
} = require("../lib/helpers/article.helpers");

const getSearchResults = (req, res) => {
  const searchTerm = req.fields.searchTerm;
  const offset = req.fields.page || 0;
  APIR.post("search/v1", {
    ...FT_REQUEST_PAYLOAD,
    queryString: searchTerm,
    resultContext: { ...FT_REQUEST_PAYLOAD.resultContext, offset }
  })
    .pipe(
      map(response => ({
        articles: response.results[0].results.map(transformArticle),
        paging: transformPaging(
          response.query.resultContext,
          response.results[0].indexCount,
          searchTerm
        )
      }))
    )
    .subscribe(
      response => {
        res.render("searchResults", {
          articles: response.articles,
          paging: response.paging
        });
      },
      error => {
        logger.errorLogger(error);
      }
    );
};

module.exports = { getSearchResults };
