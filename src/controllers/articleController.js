const { APIR } = require("../lib/API/api");
const logger = require("../lib/logs/log");

const getArticle = (req, res) => {
  const articleId = req.params.articleId;

  res.render("article");
  APIR.get(articleId).subscribe(
    response => {
      res.render("article", {
        articles: response.article
      });
    },
    error => {
      logger.errorLogger(error);
    }
  );
};

module.exports = { getArticle };
