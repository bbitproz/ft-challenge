const { Router } = require("express");
const { getArticle } = require("../controllers/articleController");

const articleRouter = Router();

articleRouter.get("/article/:articleId", getArticle);

module.exports = articleRouter;
