const { Router } = require("express");
const { getSearchResults } = require("../controllers/searchResultsController");

const searchResultsRouter = Router();

searchResultsRouter.post("/search-results", getSearchResults);

module.exports = searchResultsRouter;
