const { of, throwError } = require("rxjs");
const searchResultsController = require("../../src/controllers/searchResultsController");
const AxiosAPI = require("../../src/lib/API/api");
const { FT_REQUEST_PAYLOAD } = require("../../src/lib/payloads/ft.payload");
const mockSearchResults = require("../mocks/search.response.mock");
const logger = require("../../src/lib/logs/log");

const responseMock = {
  render: page => {}
};

const requestMock = {
  fields: {
    searchTerm: "Brexit",
    offset: 0
  }
};

const payload = {
  ...FT_REQUEST_PAYLOAD,
  queryString: requestMock.fields.searchTerm,
  resultContext: {
    ...FT_REQUEST_PAYLOAD.resultContext,
    offset: requestMock.fields.offset
  }
};

const POST_METHOD = "post";

describe("getSearchResults", () => {
  it("should send a post request with a payload", () => {
    spyOn(responseMock, "render").and.returnValue("{}");
    const apiPost = spyOn(AxiosAPI.APIR, POST_METHOD).and.returnValue({
      pipe: () => of({})
    });
    searchResultsController.getSearchResults(requestMock, responseMock);
    const firstArg = "search/v1";
    expect(apiPost).toHaveBeenCalledWith(firstArg, payload);
  });

  it("should call renderer with searchResults argument", () => {
    const resMockRender = spyOn(responseMock, "render");
    const mockReturn = { articles: mockSearchResults[0].results[0], paging: 0 };
    spyOn(AxiosAPI.APIR, POST_METHOD).and.returnValue({
      pipe: () => of(mockReturn)
    });
    searchResultsController.getSearchResults(requestMock, responseMock);
    expect(resMockRender).toHaveBeenCalledWith("searchResults", mockReturn);
  });

  it("should call renderer with error argument", () => {
    const error = "error";
    const errorLoggerSpy = spyOn(logger, "errorLogger");
    spyOn(AxiosAPI.APIR, POST_METHOD).and.returnValue(throwError("error"));
    searchResultsController.getSearchResults(requestMock, responseMock);
    expect(errorLoggerSpy).toHaveBeenCalledWith(error);
  });
});
