const { of, throwError } = require("rxjs");
const articleController = require("../../src/controllers/articleController");
const AxiosAPI = require("../../src/lib/API/api");
const logger = require("../../src/lib/logs/log");
const mockArticle = require("../mocks/article.mock");

const responseMock = {
  render: page => {}
};

const requestMock = {
  params: {
    articleId: 5
  }
};

describe("getArticle", () => {
  it("should send a request", () => {
    spyOn(responseMock, "render").and.returnValue("{}");
    const apiGet = spyOn(AxiosAPI.APIR, "get").and.returnValue({
      subscribe: () => {}
    });
    articleController.getArticle(requestMock, responseMock);
    expect(apiGet).toHaveBeenCalledWith(requestMock.params.articleId);
  });

  it("should call renderer with article argument", () => {
    const resMockRender = spyOn(responseMock, "render");
    spyOn(AxiosAPI.APIR, "get").and.returnValue(of(mockArticle));
    articleController.getArticle(requestMock, responseMock);
    expect(resMockRender).toHaveBeenCalledWith("article");
  });

  it("should call renderer with error argument", () => {
    const error = "error";
    const errorLoggerSpy = spyOn(logger, "errorLogger");
    spyOn(AxiosAPI.APIR, "get").and.returnValue(throwError(error));
    articleController.getArticle(requestMock, responseMock);
    expect(errorLoggerSpy).toHaveBeenCalledWith(error);
  });
});
