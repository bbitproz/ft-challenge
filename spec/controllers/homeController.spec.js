const { of, throwError } = require("rxjs");
const homeController = require("../../src/controllers/homeController");

const responseMock = {
  render: page => {}
};

const requestMock = {
  params: {
    articleId: 5
  }
};

describe("getHome", () => {
  it("should render home", () => {
    const resMockRender = spyOn(responseMock, "render").and.returnValue("{}");
    homeController.getHome(requestMock, responseMock);
    expect(resMockRender).toHaveBeenCalledWith("home");
  });
});
