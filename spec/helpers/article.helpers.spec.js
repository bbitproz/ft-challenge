const {
  transformArticle,
  transformPaging
} = require("../../src/lib/helpers/article.helpers");
const article = require("../mocks/article.mock");

describe("helpers", () => {
  it("transformArticle", () => {
    const transformed = transformArticle(article);
    expect(transformed.image).toEqual(article.images[0].url);
    expect(transformed.title).toEqual(article.title.title);
    expect(transformed.location).toEqual(article.location.uri);
    expect(transformed.category).toEqual(article.editorial.byline);
  });

  it("transformPaging", () => {
    const mockPaging = { offset: 0, maxResults: 20 };
    const searchTerm = "Brexit";
    const transformedPages = transformPaging(mockPaging, 100, searchTerm);
    expect(transformedPages.showPrev).toEqual(false);
    expect(transformedPages.prevOffset).toEqual(-20);
    expect(transformedPages.showNext).toEqual(true);
    expect(transformedPages.nextOffset).toEqual(20);
    expect(transformedPages.page).toEqual(1);
    expect(transformedPages.offset).toEqual(0);
    expect(transformedPages.maxitems).toEqual(20);
    expect(transformedPages.totalCount).toEqual(5);
    expect(transformedPages.searchTerm).toEqual(searchTerm);
  });
});
