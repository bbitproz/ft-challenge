const mockArticle = {
  aspectSet: "article",
  modelVersion: "1",
  id: "951a6386-6097-11ea-a6cd-df28cc3c6a68",
  apiUrl: "https://api.ft.com/content/951a6386-6097-11ea-a6cd-df28cc3c6a68",
  title: {
    title: "Lebanon to default on $1.2bn debt"
  },
  lifecycle: {
    initialPublishDateTime: "2020-03-07T18:21:52Z",
    lastPublishDateTime: "2020-03-07T18:21:52Z"
  },
  location: {
    uri: "https://www.ft.com/content/951a6386-6097-11ea-a6cd-df28cc3c6a68"
  },
  summary: {
    excerpt:
      "...liquidity-starved local banks, the politics of paying a $1.2bn Eurobond maturing this Monday had become toxic. Meanwhile a..."
  },
  editorial: {
    subheading:
      "Beirut to seek new payment terms as it struggles to cope with its worst financial and economic crisis",
    byline: "Chloe Cornish in Beirut and Tommy Stubbington in London"
  },
  images: [{ url: "http://www.google.com" }]
};

module.exports = mockArticle;
