const resultMock = [
  {
    indexCount: 190586,
    curations: ["ARTICLES", "BLOGS", "PAGES", "PODCASTS", "VIDEOS"],
    results: [
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "951a6386-6097-11ea-a6cd-df28cc3c6a68",
        apiUrl:
          "https://api.ft.com/content/951a6386-6097-11ea-a6cd-df28cc3c6a68",
        title: {
          title: "Lebanon to default on $1.2bn debt"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-07T18:21:52Z",
          lastPublishDateTime: "2020-03-07T18:21:52Z"
        },
        location: {
          uri: "https://www.ft.com/content/951a6386-6097-11ea-a6cd-df28cc3c6a68"
        },
        summary: {
          excerpt:
            "...liquidity-starved local banks, the politics of paying a $1.2bn Eurobond maturing this Monday had become toxic. Meanwhile a..."
        },
        editorial: {
          subheading:
            "Beirut to seek new payment terms as it struggles to cope with its worst financial and economic crisis",
          byline: "Chloe Cornish in Beirut and Tommy Stubbington in London"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "fca34d7f-0080-4b80-87ec-c47432887b2e",
        apiUrl:
          "https://api.ft.com/content/fca34d7f-0080-4b80-87ec-c47432887b2e",
        title: {
          title: "Time to modernise investor dispute arbitration"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-07T06:00:45Z",
          lastPublishDateTime: "2020-03-07T06:00:45Z"
        },
        location: {
          uri: "https://www.ft.com/content/fca34d7f-0080-4b80-87ec-c47432887b2e"
        },
        summary: {
          excerpt:
            "..., arbitrators cannot enforce payment of damages.Litigation financing by third parties — such as investment banks or hedge..."
        },
        editorial: {
          subheading:
            "The current system is fractured, dysfunctional and stuck in a time warp",
          byline: "Harry Broadman"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "84ba2f00-5f9b-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/84ba2f00-5f9b-11ea-8033-fa40a0d65a98",
        title: {
          title:
            "Relx could relax but riskier bond issuers may find it tough going"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-07T04:00:34Z",
          lastPublishDateTime: "2020-03-07T04:00:34Z"
        },
        location: {
          uri: "https://www.ft.com/content/84ba2f00-5f9b-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...the banks was that it wasn’t appropriate to go last week,” Nick Luff, chief financial officer at the London-headquartered..."
        },
        editorial: {
          subheading:
            "Lower-rated companies have begun to worry about the task of refinancing vast debts",
          byline: "Joe Rennison in London"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "d4590be4-5fcd-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/d4590be4-5fcd-11ea-8033-fa40a0d65a98",
        title: {
          title:
            "Rishi Sunak acts to preserve access to cash for the vulnerable"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T22:32:11Z",
          lastPublishDateTime: "2020-03-06T22:32:11Z"
        },
        location: {
          uri: "https://www.ft.com/content/d4590be4-5fcd-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...— can still access it when they want. One proposal will involve giving watchdogs new powers to force banks to continue to..."
        },
        editorial: {
          subheading:
            "Campaigners welcome changes as contactless payments become increasingly prevalent",
          byline: "Jim Pickard and Nicholas Megaw"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "98ad1fd4-5fc5-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/98ad1fd4-5fc5-11ea-b0ab-339c2307bcd4",
        title: {
          title:
            "Yes Bank — the Indian tiger which bit off more than it could chew"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T22:00:35Z",
          lastPublishDateTime: "2020-03-06T22:00:35Z"
        },
        location: {
          uri: "https://www.ft.com/content/98ad1fd4-5fc5-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "Of all the private banks set up during India’s go-go-years early in the new millennium, Yes Bank was the most ambitious..."
        },
        editorial: {
          subheading:
            "Private lender’s outsized risk appetite ultimately proved a major vulnerability",
          byline: "Amy Kazmin in New Delhi and Benjamin Parkin in Mumbai"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "e829238c-5fd9-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/e829238c-5fd9-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Joining the stampede for US Treasuries"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T21:52:21Z",
          lastPublishDateTime: "2020-03-06T21:52:21Z"
        },
        location: {
          uri: "https://www.ft.com/content/e829238c-5fd9-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "..., and it pretty much rests on central banks trying to calm financial sentiment. Mark Dowding at BlueBay Asset Management..."
        },
        editorial: {
          subheading:
            "Mike Mackenzie’s daily analysis of what’s moving global markets ",
          byline: "Michael Mackenzie"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "30b51ac0-b504-11e3-af92-00144feabdc0",
        apiUrl:
          "https://api.ft.com/content/30b51ac0-b504-11e3-af92-00144feabdc0",
        title: {
          title: "Latest savings rates"
        },
        lifecycle: {
          initialPublishDateTime: "2014-03-31T13:52:26Z",
          lastPublishDateTime: "2020-03-06T18:52:53Z"
        },
        location: {
          uri: "https://www.ft.com/content/30b51ac0-b504-11e3-af92-00144feabdc0"
        },
        summary: {
          excerpt:
            "Are you getting the best deal on your building society and bank savings? Check the latest interest rates on accounts..."
        },
        editorial: {
          subheading: "Up-to-date deals from banks and building societies"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "606f1c8c-5f96-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/606f1c8c-5f96-11ea-8033-fa40a0d65a98",
        title: {
          title:
            "Coronavirus: why central bankers say it is time for fiscal stimulus"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T18:12:35Z",
          lastPublishDateTime: "2020-03-06T18:12:35Z"
        },
        location: {
          uri: "https://www.ft.com/content/606f1c8c-5f96-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "..., central banks jump in because they are on the losing end of a game of chicken: they act, even with imperfect..."
        },
        editorial: {
          subheading:
            "Many policymakers are calling for more government spending in response to economic disruption",
          byline:
            "Robin Harding in Tokyo, Brendan Greeley in Washington and Martin Arnold in Frankfurt"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "68c242a3-87ce-4db6-9a39-611ac0fdcaf8",
        apiUrl:
          "https://api.ft.com/content/68c242a3-87ce-4db6-9a39-611ac0fdcaf8",
        title: {
          title: "JPMorgan/Jamie Dimon: soldiering on"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T17:59:04Z",
          lastPublishDateTime: "2020-03-06T17:59:04Z"
        },
        location: {
          uri: "https://www.ft.com/content/68c242a3-87ce-4db6-9a39-611ac0fdcaf8"
        },
        summary: {
          excerpt:
            "There is never a good time for a business leader to fall ill. For JPMorgan Chase, news that its longtime chief executive..."
        },
        editorial: {
          subheading:
            "Chief’s health scare is a rude reminder that nothing is forever"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "045bf4d9-5c1c-4932-90ad-ab6c2daa2c65",
        apiUrl:
          "https://api.ft.com/content/045bf4d9-5c1c-4932-90ad-ab6c2daa2c65",
        title: {
          title: "Peter Hargreaves urges investors to stay the course"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T17:01:46Z",
          lastPublishDateTime: "2020-03-06T17:01:46Z"
        },
        location: {
          uri: "https://www.ft.com/content/045bf4d9-5c1c-4932-90ad-ab6c2daa2c65"
        },
        summary: {
          excerpt:
            "...economic impact of the virus. Following the US Federal Reserve’s emergency rate cut on Tuesday, central banks around the..."
        },
        editorial: {
          subheading:
            "Billionaire platform founder says he has rarely witnessed timely reinvestment in market volatility",
          byline: "Madison Darbyshire"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "55538a00-5f3b-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/55538a00-5f3b-11ea-8033-fa40a0d65a98",
        title: {
          title: "JPMorgan chief Jamie Dimon has emergency heart surgery"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T23:51:08Z",
          lastPublishDateTime: "2020-03-06T15:30:42Z"
        },
        location: {
          uri: "https://www.ft.com/content/55538a00-5f3b-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "Jamie Dimon has spoken to senior colleagues and “feels really good” after emergency heart surgery, according to a person..."
        },
        editorial: {
          subheading:
            "US bank’s CEO says he ‘feels really good’ after medical procedure",
          byline: "Laura Noonan in New York"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "76b7f6ae-5fb8-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/76b7f6ae-5fb8-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Best of Lex: your weekly round-up"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T15:18:37Z",
          lastPublishDateTime: "2020-03-06T15:18:37Z"
        },
        location: {
          uri: "https://www.ft.com/content/76b7f6ae-5fb8-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...and fixed income trading. Big diversified Wall Street banks will benefit. Expect the likes of JPMorgan Chase and Morgan..."
        },
        editorial: {
          subheading:
            "Asian food stocks, Asian telecoms, US banks, Asian insurers, Flybe, rate cuts, Waymo, Twitter, GE",
          byline: "Vanessa Houlder"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "0e4e46f4-5f9f-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/0e4e46f4-5f9f-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Shares tumble in London-listed parent of Russia bank Tinkoff"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T14:25:25Z",
          lastPublishDateTime: "2020-03-06T15:11:47Z"
        },
        location: {
          uri: "https://www.ft.com/content/0e4e46f4-5f9f-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "Shares in TCS, the London-listed parent company of Russian online bank Tinkoff, closed down almost 24 per cent on Friday..."
        },
        editorial: {
          subheading:
            "Founder Oleg Tinkov reveals he has cancer as he faces up to six years jail for US tax evasion",
          byline: "Max Seddon in Moscow"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "b5c36d2a-5fa3-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/b5c36d2a-5fa3-11ea-8033-fa40a0d65a98",
        title: {
          title: "Investors stunned by blowout bond rally"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T14:29:17Z",
          lastPublishDateTime: "2020-03-06T14:29:17Z"
        },
        location: {
          uri: "https://www.ft.com/content/b5c36d2a-5fa3-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...central banks following the Federal Reserve’s emergency rate cut on Tuesday. The pace of the rally suggested an..."
        },
        editorial: {
          subheading:
            "Fund managers rush for safety of government debt at quickest pace since crisis of 2008",
          byline: "Tommy Stubbington, Robin Wigglesworth and Colby Smith"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "ef950c8c-5e6a-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/ef950c8c-5e6a-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Very dark signals are coming from bond markets"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T13:40:14Z",
          lastPublishDateTime: "2020-03-06T13:40:14Z"
        },
        location: {
          uri: "https://www.ft.com/content/ef950c8c-5e6a-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...easing from central banks in Canada and Australia, reflects efforts to get ahead of tightening financial conditions. The..."
        },
        editorial: {
          subheading:
            "Damage to stocks from coronavirus crisis has been modest compared with fixed income",
          byline: "Michael Mackenzie"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "f25dbda0-5ecf-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/f25dbda0-5ecf-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Coronavirus mayhem reflects phenomenon of ‘shock-led’ markets"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T13:31:16Z",
          lastPublishDateTime: "2020-03-06T13:31:16Z"
        },
        location: {
          uri: "https://www.ft.com/content/f25dbda0-5ecf-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...investors getting in on the game. Option dealers, usually banks, take the other side of the trades. ..."
        },
        editorial: {
          subheading:
            "Investors need to get to grips with stronger, longer booms and grim but rapid busts",
          byline: "Robin Wigglesworth"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "63d5aebc-5fa1-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/63d5aebc-5fa1-11ea-8033-fa40a0d65a98",
        title: {
          title:
            "Lebanon’s state prosecutor overturns freeze on assets of 20 banks"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T13:13:48Z",
          lastPublishDateTime: "2020-03-06T13:13:48Z"
        },
        location: {
          uri: "https://www.ft.com/content/63d5aebc-5fa1-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...almost half the country’s banks, in a move that exposed the financial sector’s worsening vulnerability. The..."
        },
        editorial: {
          subheading:
            "Order and counter-judgment exposes financial sector’s worsening vulnerability",
          byline: "Chloe Cornish in Beirut"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "7481b6fa-5f4e-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/7481b6fa-5f4e-11ea-8033-fa40a0d65a98",
        title: {
          title: "Banks seek trading rule guidance as coronavirus spreads"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T12:51:04Z",
          lastPublishDateTime: "2020-03-06T12:51:04Z"
        },
        location: {
          uri: "https://www.ft.com/content/7481b6fa-5f4e-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "Global banks are pushing for guidance from regulators over the exemptions and temporary permissions they will need to..."
        },
        editorial: {
          subheading:
            "Regulators urged to provide clarity on exemptions ahead of potential staff quarantines",
          byline:
            "Laura Noonan in New York, Nicholas Megaw in London and Primrose Riordan in Hong Kong"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "daa3ba7a-5f3d-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/daa3ba7a-5f3d-11ea-b0ab-339c2307bcd4",
        title: {
          title:
            "Hiro Mizuno rallies pension funds for ESG; fast fashion wears thin; green financing gallops ahead"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T12:44:42Z",
          lastPublishDateTime: "2020-03-06T12:44:42Z"
        },
        location: {
          uri: "https://www.ft.com/content/daa3ba7a-5f3d-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...reality they are not”, he said.  Banks need to be careful not to push the green halo on too many of their clients or they..."
        },
        editorial: {
          subheading:
            "Your guide to the investment and business revolution you can’t afford to ignore ",
          byline:
            "Gillian Tett, Patrick Temple-West and Andrew Edgecliffe-Johnson"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "68a4141e-5f07-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/68a4141e-5f07-11ea-b0ab-339c2307bcd4",
        title: {
          title: "India forced to rescue big private sector bank"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T19:18:36Z",
          lastPublishDateTime: "2020-03-06T11:06:32Z"
        },
        location: {
          uri: "https://www.ft.com/content/68a4141e-5f07-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "India’s central bank has seized control of the country’s fourth-largest private lender, enlisting the top state bank to..."
        },
        editorial: {
          subheading:
            "RBI urges Yes Bank depositors not to panic as crisis sparks concern over financial system",
          byline: "Benjamin Parkin in Mumbai and Amy Kazmin in New Delhi"
        }
      },
      {
        aspectSet: "video",
        modelVersion: "1",
        id: "0c9a7891-296c-4832-a47f-3d859033f47d",
        apiUrl:
          "https://api.ft.com/content/0c9a7891-296c-4832-a47f-3d859033f47d",
        title: {
          title: "Friday, March 6"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T05:01:00Z",
          lastPublishDateTime: "2020-03-06T05:01:00Z"
        },
        location: {
          uri: "https://www.ft.com/content/0c9a7891-296c-4832-a47f-3d859033f47d"
        },
        editorial: {
          byline:
            "A rundown of the most important global business stories you need to know for the coming day, from the newsroom of the Financial Times. Available every weekday morning."
        },
        summary: {
          excerpt:
            "US stocks dropped as Treasury yields touched records lows on Thursday and bank share price falls led the way, struggling..."
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "0101df94-5e3f-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/0101df94-5e3f-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Strasbourg is test case for rise of the Greens in France"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T05:00:50Z",
          lastPublishDateTime: "2020-03-06T05:00:50Z"
        },
        location: {
          uri: "https://www.ft.com/content/0101df94-5e3f-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "Strasbourg, on the banks of the Rhine, is becoming a test case for the ability of France’s Green party, Europe Écologie..."
        },
        editorial: {
          subheading:
            "Mayoral elections will show if party can transform itself into a genuine political force",
          byline: "David Keohane in Strasbourg"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "0b8ea632-ebc5-4c39-a386-cc613596d3b7",
        apiUrl:
          "https://api.ft.com/content/0b8ea632-ebc5-4c39-a386-cc613596d3b7",
        title: {
          title: "Cash Isas: only worth it for the wealthiest"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T05:00:13Z",
          lastPublishDateTime: "2020-03-06T05:00:13Z"
        },
        location: {
          uri: "https://www.ft.com/content/0b8ea632-ebc5-4c39-a386-cc613596d3b7"
        },
        summary: {
          excerpt:
            "Attractive deals on cash Isas are thin on the ground as inflation ticks up and banks have balked at bringing out market..."
        },
        editorial: {
          subheading:
            "Puny interest rates have dented the tax sheltering properties of cash Isas for most ",
          byline: "James Pickford"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "e1f5df86-36af-4fa2-9cdd-666ffa524926",
        apiUrl:
          "https://api.ft.com/content/e1f5df86-36af-4fa2-9cdd-666ffa524926",
        title: {
          title: "China’s post-virus stimulus: no silver bullet"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T05:00:13Z",
          lastPublishDateTime: "2020-03-06T05:00:13Z"
        },
        location: {
          uri: "https://www.ft.com/content/e1f5df86-36af-4fa2-9cdd-666ffa524926"
        },
        summary: {
          excerpt:
            "...ability to repay debt could be jeopardised by declining income. Banks have been encouraged to offer debt..."
        },
        editorial: {
          subheading: "The world should not expect another bailout by Beijing",
          byline: "David Lubin"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "8f2b22fc-88d9-4fac-84b5-ded23aadad4a",
        apiUrl:
          "https://api.ft.com/content/8f2b22fc-88d9-4fac-84b5-ded23aadad4a",
        title: {
          title: "Fed Funds cuts are for TV news and headlines"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T05:00:13Z",
          lastPublishDateTime: "2020-03-06T05:00:13Z"
        },
        location: {
          uri: "https://www.ft.com/content/8f2b22fc-88d9-4fac-84b5-ded23aadad4a"
        },
        summary: {
          excerpt:
            "...wave on to the banks’ balance sheets? Soon, maybe not.For fixed-income investors, that probably means letting someone..."
        },
        editorial: {
          subheading:
            "The average investor still believes their portfolio can be saved by moderate Democrats, or the FOMC",
          byline: "John Dizard"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "41eff966-4dc6-11ea-95a0-43d18ec715f5",
        apiUrl:
          "https://api.ft.com/content/41eff966-4dc6-11ea-95a0-43d18ec715f5",
        title: {
          title: "Career planning: would a coach put me back on the right path?"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T04:30:50Z",
          lastPublishDateTime: "2020-03-06T04:30:50Z"
        },
        location: {
          uri: "https://www.ft.com/content/41eff966-4dc6-11ea-95a0-43d18ec715f5"
        },
        summary: {
          excerpt:
            "...crisis. Friends two years ahead of me had walked into jobs in investment banks, top law firms and management..."
        },
        editorial: {
          subheading:
            "‘Drunk spider’ diagram revealed my true weaknesses and strengths",
          byline: "Kate Hodge"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "87b61c4e-4e54-11ea-95a0-43d18ec715f5",
        apiUrl:
          "https://api.ft.com/content/87b61c4e-4e54-11ea-95a0-43d18ec715f5",
        title: {
          title:
            "How I got here: Morgan Stanley’s Clare Woodman’s career choices"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T04:30:27Z",
          lastPublishDateTime: "2020-03-06T04:30:27Z"
        },
        location: {
          uri: "https://www.ft.com/content/87b61c4e-4e54-11ea-95a0-43d18ec715f5"
        },
        summary: {
          excerpt:
            "...the male-dominated world of Wall Street banks and the City. Role models Now Morgan Stanley’s chief executive of Europe..."
        },
        editorial: {
          subheading:
            "Bank’s head of Emea talks about ambition, strategy and help from a ‘third parent’",
          byline: "Laura Noonan, in New York"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "c9dc4968-517e-11ea-a1ef-da1721a0541e",
        apiUrl:
          "https://api.ft.com/content/c9dc4968-517e-11ea-a1ef-da1721a0541e",
        title: {
          title:
            "Overconfident investors need diligent advisers who can alert them to threats"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T04:00:33Z",
          lastPublishDateTime: "2020-03-06T04:00:33Z"
        },
        location: {
          uri: "https://www.ft.com/content/c9dc4968-517e-11ea-a1ef-da1721a0541e"
        },
        summary: {
          excerpt:
            "...belief that cheap money has pushed prices too high. To Tavazzi, “a world where central banks keep the cost of funding low..."
        },
        editorial: {
          subheading:
            "Fear of missing out on returns can overcome caution even with markets at record highs",
          byline: "Matthew Vincent"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "3199511c-5ec8-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/3199511c-5ec8-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Thyssenkrupp turns tables in private equity megadeal"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T04:00:28Z",
          lastPublishDateTime: "2020-03-06T04:00:28Z"
        },
        location: {
          uri: "https://www.ft.com/content/3199511c-5ec8-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...borrower can pay interest with further debt. If the buyers go ahead with that, banks are looking to place the riskiest..."
        },
        editorial: {
          subheading:
            "German group pulls off coup as buyout funds pay eye-watering sum for lift business",
          byline: "Kaye Wiggins, Joe Miller and Robert Smith"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "6d1725be-5e9d-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/6d1725be-5e9d-11ea-b0ab-339c2307bcd4",
        title: {
          title: "China’s central bank resists large-scale coronavirus stimulus"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T01:00:26Z",
          lastPublishDateTime: "2020-03-06T01:00:26Z"
        },
        location: {
          uri: "https://www.ft.com/content/6d1725be-5e9d-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...“powerful tool to lower banks’ funding costs”, which have risen over the past three years.  The PBoC is also concerned..."
        },
        editorial: {
          subheading:
            "Policymakers unmoved by US Fed’s surprise rate cut, preferring tailored measures",
          byline: "Tom Mitchell in Singapore"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "7fb1e1fe-5f39-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/7fb1e1fe-5f39-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Global equity outflows hit $23bn on coronavirus fears"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-06T00:50:38Z",
          lastPublishDateTime: "2020-03-06T00:50:38Z"
        },
        location: {
          uri: "https://www.ft.com/content/7fb1e1fe-5f39-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...lowering of its policy rate followed supportive statements from finance ministers and central banks across the globe..."
        },
        editorial: {
          subheading:
            "Europe-focused funds have their worst week since July 2016",
          byline: "Jennifer Ablan in New York and Joe Rennison in London"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "30529a16-5f18-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/30529a16-5f18-11ea-b0ab-339c2307bcd4",
        title: {
          title: "US banks, transport stocks and small-caps enter bear market"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T21:59:14Z",
          lastPublishDateTime: "2020-03-05T21:59:14Z"
        },
        location: {
          uri: "https://www.ft.com/content/30529a16-5f18-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "Two of the US equity market’s most economically sensitive sectors have flashed a warning signal, with measures of both..."
        },
        editorial: {
          subheading:
            "Economically sensitive sectors remain under pressure from Fed rate cut, virus woes",
          byline: "Peter Wells in New York"
        }
      },
      {
        aspectSet: "video",
        modelVersion: "1",
        id: "40e8ddd6-dcc7-4590-88ed-c409b6a5861a",
        apiUrl:
          "https://api.ft.com/content/40e8ddd6-dcc7-4590-88ed-c409b6a5861a",
        title: {
          title:
            "Coronavirus: how to read the market reaction | Charts that Count"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T21:32:45Z",
          lastPublishDateTime: "2020-03-05T21:32:45Z"
        },
        location: {
          uri: "https://www.ft.com/content/40e8ddd6-dcc7-4590-88ed-c409b6a5861a"
        },
        editorial: {
          subheading:
            "The FT's US finance editor Robert Armstrong breaks down the S&P 500",
          byline: "Produced by Gregory Bobillot"
        },
        summary: {
          excerpt:
            "The FT's US finance editor Robert Armstrong breaks down the S&P 500 to show how stocks in healthcare, utilities..."
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "ba1a20b6-5f13-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/ba1a20b6-5f13-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Strapping in tighter for a wilder ride"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T21:30:25Z",
          lastPublishDateTime: "2020-03-05T21:30:25Z"
        },
        location: {
          uri: "https://www.ft.com/content/ba1a20b6-5f13-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...companies has also broken lower today, suggesting a bigger correction is under way. The KBW index of leading..."
        },
        editorial: {
          subheading:
            "Mike Mackenzie’s daily analysis of what’s moving global markets",
          byline: "Michael Mackenzie"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "821c6ecc-5e86-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/821c6ecc-5e86-11ea-b0ab-339c2307bcd4",
        title: {
          title: "US stocks drop as US Treasury yields hit record lows"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T02:54:12Z",
          lastPublishDateTime: "2020-03-05T21:22:00Z"
        },
        location: {
          uri: "https://www.ft.com/content/821c6ecc-5e86-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...March 17-18 and another 25bp reduction in April. “Eventually, additional actions to support banks and ensure ample..."
        },
        editorial: {
          subheading:
            "S&P 500 closes down more than 3 per cent as traders await another Fed rate cut",
          byline:
            "Colby Smith and Richard Henderson in New York, Philip Georgiadis in London and Hudson Lockett in Hong Kong"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "d3741b8e-5ee6-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/d3741b8e-5ee6-11ea-8033-fa40a0d65a98",
        title: {
          title: "Coronavirus shake-up is changing City behaviour"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T21:19:36Z",
          lastPublishDateTime: "2020-03-05T21:19:36Z"
        },
        location: {
          uri: "https://www.ft.com/content/d3741b8e-5ee6-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "When Goldman Sachs’ finest must commute to Croydon, JPMorgan bankers need to brunch in Basingstoke, and Lloyd’s of..."
        },
        editorial: {
          subheading:
            "The spread of Covid-19 is resulting in unusual etiquette in unusual places",
          byline: "Matthew Vincent"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "f80088e4-5f0d-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/f80088e4-5f0d-11ea-8033-fa40a0d65a98",
        title: {
          title:
            "Credit Suisse linked to list of Nazi émigré accounts in Argentina"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T20:38:04Z",
          lastPublishDateTime: "2020-03-05T20:38:04Z"
        },
        location: {
          uri: "https://www.ft.com/content/f80088e4-5f0d-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...bank’s headquarters in Zurich are still emblazoned with its acronym: SKA. Switzerland’s banks have done extensive work..."
        },
        editorial: {
          subheading:
            "‘Many’ of the 12,000 German fascists named used Swiss bank’s predecessor to funnel looted Jewish money",
          byline: "Sam Jones in Zurich"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "19581d72-5eff-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/19581d72-5eff-11ea-8033-fa40a0d65a98",
        title: {
          title:
            "Lebanon’s financial prosecutor orders asset freeze on 20 banks"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T20:01:36Z",
          lastPublishDateTime: "2020-03-05T20:01:36Z"
        },
        location: {
          uri: "https://www.ft.com/content/19581d72-5eff-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "Lebanon’s financial prosecutor has frozen the assets of almost half the crisis-hit country’s banks and their executives..."
        },
        editorial: {
          subheading:
            "Move piles further pressure on crisis-hit country’s stressed financial sector",
          byline: "Chloe Cornish in Beirut"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "b95fb692-5ed5-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/b95fb692-5ed5-11ea-b0ab-339c2307bcd4",
        title: {
          title: "HSBC sends home London research staff after coronavirus case"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T11:52:54Z",
          lastPublishDateTime: "2020-03-05T18:11:48Z"
        },
        location: {
          uri: "https://www.ft.com/content/b95fb692-5ed5-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...affecting their staff and IT systems. The letter, sent to 117 banks, asked them to immediately inform the ECB..."
        },
        editorial: {
          subheading:
            "Bank evacuates section of Canary Wharf office for deep cleaning as employee tests positive",
          byline: "Stephen Morris in London and Laura Noonan in New York"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "a3c3c66d-d180-4bfb-ac6a-ef8f6e535b05",
        apiUrl:
          "https://api.ft.com/content/a3c3c66d-d180-4bfb-ac6a-ef8f6e535b05",
        title: {
          title: "US banks/rates: Fortress Wall Street"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T18:09:13Z",
          lastPublishDateTime: "2020-03-05T18:09:13Z"
        },
        location: {
          uri: "https://www.ft.com/content/a3c3c66d-d180-4bfb-ac6a-ef8f6e535b05"
        },
        summary: {
          excerpt:
            "What a difference two months make. Coming out of fourth-quarter earnings in January, US banks were largely optimistic..."
        },
        editorial: {
          subheading:
            "Nine largest banks by assets have collectively seen $287bn wiped off their value. "
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "4102d7f4-5edc-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/4102d7f4-5edc-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Boris Johnson plans UK economic response to coronavirus"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T16:29:46Z",
          lastPublishDateTime: "2020-03-05T16:29:46Z"
        },
        location: {
          uri: "https://www.ft.com/content/4102d7f4-5edc-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...overdrafts or offering repayment relief on loans. Internally, the BoE is working on a plan to provide banks..."
        },
        editorial: {
          subheading:
            "Prime minister holds talks with chancellor and BoE governor to discuss measures",
          byline: "George Parker, Laura Hughes and Chris Giles in London"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "38ba602c-5e27-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/38ba602c-5e27-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Fears over coronavirus leave investors huddling in utilities"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T16:18:38Z",
          lastPublishDateTime: "2020-03-05T16:18:38Z"
        },
        location: {
          uri: "https://www.ft.com/content/38ba602c-5e27-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...central banks have doubled down on monetary easing measures to alleviate the effects of the outbreak. The lower the yields..."
        },
        editorial: {
          subheading:
            "Demand for classically defensive stocks rises as viral outbreak worsens",
          byline: "Anna Gross in London"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "6d1d5e7a-5ede-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/6d1d5e7a-5ede-11ea-b0ab-339c2307bcd4",
        title: {
          title: "SBI Cards IPO draws strong demand from investors"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T15:56:41Z",
          lastPublishDateTime: "2020-03-05T15:56:41Z"
        },
        location: {
          uri: "https://www.ft.com/content/6d1d5e7a-5ede-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "The State Bank of India’s credit card company is going public in one of the country’s few billion-dollar flotations in..."
        },
        editorial: {
          subheading:
            "India’s second-largest card issuer seeks to defy bearish mood with $1.4bn float",
          byline: "Benjamin Parkin and Henny Sender in Mumbai"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "e0a8af3c-5e54-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/e0a8af3c-5e54-11ea-b0ab-339c2307bcd4",
        title: {
          title:
            "Don’t give up on the Fed’s virus-fighting capabilities just yet"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T12:46:56Z",
          lastPublishDateTime: "2020-03-05T12:46:56Z"
        },
        location: {
          uri: "https://www.ft.com/content/e0a8af3c-5e54-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...-ordinating with other central banks looked haphazard, and doing so reminded investors just how little firepower..."
        },
        editorial: {
          subheading:
            "Emergency rates cut causes alarm in markets after initial spurt of excitement",
          byline: "Robin Wigglesworth"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "93782dac-5e34-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/93782dac-5e34-11ea-b0ab-339c2307bcd4",
        title: {
          title: "FirstFT: Today’s top stories"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T22:50:55Z",
          lastPublishDateTime: "2020-03-05T12:32:10Z"
        },
        location: {
          uri: "https://www.ft.com/content/93782dac-5e34-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...increase capital requirements for the biggest banks by around $46bn but held off on countercyclical capital buffers. The..."
        },
        editorial: {
          subheading: "Your daily briefing on the news",
          byline: "Gordon Smith, Eli Meixler and Emily Goldberg"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "f97f0770-5a4b-11ea-abe5-8e03987b7b20",
        apiUrl:
          "https://api.ft.com/content/f97f0770-5a4b-11ea-abe5-8e03987b7b20",
        title: {
          title: "How central banks should think about climate change"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T10:45:26Z",
          lastPublishDateTime: "2020-03-05T10:45:26Z"
        },
        location: {
          uri: "https://www.ft.com/content/f97f0770-5a4b-11ea-abe5-8e03987b7b20"
        },
        summary: {
          excerpt:
            "...central banks’ risk assessments. Her op-ed coincided with the launch of the “COP26 Private Finance Agenda” supported by..."
        },
        editorial: {
          subheading:
            "Use monetary power to target the ‘green spread’ in the market",
          byline: "Martin Sandbu"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "1c7b1755-175f-43c0-86a8-d9d6c1443809",
        apiUrl:
          "https://api.ft.com/content/1c7b1755-175f-43c0-86a8-d9d6c1443809",
        title: {
          title: "Is data this deflationary?"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T09:57:58Z",
          lastPublishDateTime: "2020-03-05T09:57:58Z"
        },
        location: {
          uri: "https://www.ft.com/content/1c7b1755-175f-43c0-86a8-d9d6c1443809"
        },
        summary: {
          excerpt:
            "Earlier this year we explained why even though your new iPhone might cost more than your old one in nominal terms, in..."
        },
        editorial: {
          subheading:
            "Fed economists claim inflation is much lower when you count the falling cost of data use. ",
          byline: "Claire Jones"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "6e515e5c-5eb8-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/6e515e5c-5eb8-11ea-8033-fa40a0d65a98",
        title: {
          title:
            "Opening Quote: Entertainment, travel and recruitment groups hit by the virus"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T08:39:58Z",
          lastPublishDateTime: "2020-03-05T08:39:58Z"
        },
        location: {
          uri: "https://www.ft.com/content/6e515e5c-5eb8-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...mandate in April 2021. Banks are sending hundreds of staff to their UK and US disaster recovery sites, installing big..."
        },
        editorial: {
          subheading:
            "Your morning City briefing on companies in the news, job moves and what’s happening in the markets",
          byline: "Cat Rutter Pooley"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "e9666b97-2143-4465-994d-5e3045565c81",
        apiUrl:
          "https://api.ft.com/content/e9666b97-2143-4465-994d-5e3045565c81",
        title: {
          title: "Further reading"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T07:46:24Z",
          lastPublishDateTime: "2020-03-05T07:46:24Z"
        },
        location: {
          uri: "https://www.ft.com/content/e9666b97-2143-4465-994d-5e3045565c81"
        },
        summary: {
          excerpt:
            "...hits Amazon.-- And how are Coronavirus losses going to be apportioned?-- Central banks and the coronavirus.-- Coronavirus..."
        },
        editorial: {
          subheading:
            "Debunking blockchain; Coronavirus tales; a French wine slump... Plus more. ",
          byline: "Izabella Kaminska"
        }
      },
      {
        aspectSet: "video",
        modelVersion: "1",
        id: "2b6687d9-4ea5-477b-9e4b-1c8b2369cce0",
        apiUrl:
          "https://api.ft.com/content/2b6687d9-4ea5-477b-9e4b-1c8b2369cce0",
        title: {
          title: "Thursday, March 5"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T05:01:00Z",
          lastPublishDateTime: "2020-03-05T05:01:00Z"
        },
        location: {
          uri: "https://www.ft.com/content/2b6687d9-4ea5-477b-9e4b-1c8b2369cce0"
        },
        editorial: {
          byline:
            "A rundown of the most important global business stories you need to know for the coming day, from the newsroom of the Financial Times. Available every weekday morning."
        },
        summary: {
          excerpt:
            "...one of Africa’s fastest-growing economies. Plus, banks are gearing up in the event that the coronavirus outbreak forces..."
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "74484116-5e24-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/74484116-5e24-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Fed decision to go it alone bucks history of collaboration"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T05:00:37Z",
          lastPublishDateTime: "2020-03-05T05:00:37Z"
        },
        location: {
          uri: "https://www.ft.com/content/74484116-5e24-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...October 2008 the world’s leading central banks cut interest rates simultaneously in a bid to halt the gathering storm in..."
        },
        editorial: {
          subheading:
            "Policymakers who co-ordinated action in past will introduce virus measures separately",
          byline:
            "Chris Giles in London, Robin Harding in Tokyo, Martin Arnold in Frankfurt and Brendan Greeley in Washington"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "57001260-5c98-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/57001260-5c98-11ea-8033-fa40a0d65a98",
        title: {
          title: "Dark Towers — an exposé of banking gone bad"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T05:00:29Z",
          lastPublishDateTime: "2020-03-05T05:00:29Z"
        },
        location: {
          uri: "https://www.ft.com/content/57001260-5c98-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...the time: big international banks, it was said, would enjoy economies of scale and provide one-stop shopping for..."
        },
        editorial: {
          subheading:
            "David Enrich’s salutary tale of Deutsche Bank’s overreaching ambitions",
          byline: "Gary Silverman"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "751602ae-3879-4697-8ddb-9048a5585b43",
        apiUrl:
          "https://api.ft.com/content/751602ae-3879-4697-8ddb-9048a5585b43",
        title: {
          title: "Climate change tops agenda at UK voting season"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T05:00:16Z",
          lastPublishDateTime: "2020-03-05T05:00:16Z"
        },
        location: {
          uri: "https://www.ft.com/content/751602ae-3879-4697-8ddb-9048a5585b43"
        },
        summary: {
          excerpt:
            "...their workforce. Several chief executives of UK banks, including Lloyds and Barclays, have voluntarily cut their..."
        },
        editorial: {
          subheading:
            "Executive pay and diversity also remain in the spotlight for annual meetings",
          byline: "Chris Cummings"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "6ca1d9c9-9d13-49c6-96fd-38cd47bcaad2",
        apiUrl:
          "https://api.ft.com/content/6ca1d9c9-9d13-49c6-96fd-38cd47bcaad2",
        title: {
          title: "Online gambling spikes in Britain"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T05:00:16Z",
          lastPublishDateTime: "2020-03-05T05:00:16Z"
        },
        location: {
          uri: "https://www.ft.com/content/6ca1d9c9-9d13-49c6-96fd-38cd47bcaad2"
        },
        summary: {
          excerpt:
            "...free and impartial debt advice from a charity-run service.” How can consumers protect themselves?Banks are increasingly..."
        },
        editorial: {
          subheading:
            "Gambling Commission to ban people from using credit cards to bet",
          byline: "Leke Oso Alabi"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "38829e98-5180-11ea-a1ef-da1721a0541e",
        apiUrl:
          "https://api.ft.com/content/38829e98-5180-11ea-a1ef-da1721a0541e",
        title: {
          title:
            "Which US president is best for stock markets — a Republican or a Democrat?"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T04:00:29Z",
          lastPublishDateTime: "2020-03-05T04:00:29Z"
        },
        location: {
          uri: "https://www.ft.com/content/38829e98-5180-11ea-a1ef-da1721a0541e"
        },
        summary: {
          excerpt:
            "...concern among investors that central banks have run out of levers to stimulate the economy,” says Sunaina Sinha..."
        },
        editorial: {
          subheading:
            "Markets have favoured Republicans, but investors have done better under Democrats",
          byline: "Matthew Vincent"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "fed07438-5e39-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/fed07438-5e39-11ea-8033-fa40a0d65a98",
        title: {
          title:
            "Investors in US Treasuries contemplate yields tumbling to zero"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T04:00:29Z",
          lastPublishDateTime: "2020-03-05T04:00:29Z"
        },
        location: {
          uri: "https://www.ft.com/content/fed07438-5e39-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...to equivalents in Europe, where the ECB has applied a negative rate to commercial banks’ excess deposits since June 2014..."
        },
        editorial: {
          subheading:
            "Markets expect half-point rate cut from the Fed this week to be followed by more",
          byline: "Colby Smith in New York"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "b21616a6-5e35-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/b21616a6-5e35-11ea-8033-fa40a0d65a98",
        title: {
          title: "Michael Sherwood joins Revolut board"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T00:02:21Z",
          lastPublishDateTime: "2020-03-05T00:02:21Z"
        },
        location: {
          uri: "https://www.ft.com/content/b21616a6-5e35-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...and philanthropy. Mr Wilson worked in risk roles at banks including Royal Bank of Scotland and Abbey National, before..."
        },
        editorial: {
          subheading:
            "Appointment of former Goldman Sachs executive follows fundraising valuing fintech at $5.5bn",
          byline: "Nicholas Megaw, Retail Banking Correspondent"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "6bdc1e98-5d80-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/6bdc1e98-5d80-11ea-8033-fa40a0d65a98",
        title: {
          title: "Letter: Hohn has total support from Standard Chartered"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-05T00:01:28Z",
          lastPublishDateTime: "2020-03-05T00:01:28Z"
        },
        location: {
          uri: "https://www.ft.com/content/6bdc1e98-5d80-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "Further to your report “ TCI threatens banks over coal funding” (March 2): we wholly support the perspectives of Sir..."
        },
        editorial: {
          subheading: "From José Viñals, Group Chairman, Standard Chartered"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "e44bbc3e-5e3f-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/e44bbc3e-5e3f-11ea-8033-fa40a0d65a98",
        title: {
          title: "Corporate bond sales resume after week-long hiatus"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T22:59:03Z",
          lastPublishDateTime: "2020-03-04T22:59:03Z"
        },
        location: {
          uri: "https://www.ft.com/content/e44bbc3e-5e3f-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...followed supportive statements from finance ministers and central banks across the globe, as the G7 countries co-ordinated..."
        },
        editorial: {
          subheading:
            "Debt deals strong after sell-off triggered by coronavirus fears",
          byline: "Joe Rennison in London and Eric Platt in New York"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "c70fb2a8-5dc0-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/c70fb2a8-5dc0-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Wall Street rebounds to close 4% higher"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T03:21:48Z",
          lastPublishDateTime: "2020-03-04T22:06:49Z"
        },
        location: {
          uri: "https://www.ft.com/content/c70fb2a8-5dc0-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...a more co-ordinated policy response by governments and central banks around the world. The S&P 500 finished the day 4.2..."
        },
        editorial: {
          subheading:
            "Investors anticipate fresh policy responses to the spread of coronavirus",
          byline:
            "Richard Henderson in New York, Philip Georgiadis in London and Hudson Lockett in Hong Kong"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "cb0fcc9e-5e60-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/cb0fcc9e-5e60-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Fed finalises new capital rules for large US banks"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T22:00:19Z",
          lastPublishDateTime: "2020-03-04T22:00:19Z"
        },
        location: {
          uri: "https://www.ft.com/content/cb0fcc9e-5e60-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "US regulators have finalised rules that will increase capital requirements for the country’s biggest banks by around..."
        },
        editorial: {
          subheading:
            "Under new regime top lenders’ capital requirements to rise by $46bn",
          byline: "Laura Noonan in New York"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "70a2eb90-5e3f-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/70a2eb90-5e3f-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Bonds and equities paint a contrasting picture"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T21:19:33Z",
          lastPublishDateTime: "2020-03-04T21:19:33Z"
        },
        location: {
          uri: "https://www.ft.com/content/70a2eb90-5e3f-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "FT subscribers can click here to receive Market Forces every day by email. Capitulation defines a bottom in markets and..."
        },
        editorial: {
          subheading:
            "Mike Mackenzie’s daily analysis of what’s moving global markets",
          byline: "Michael Mackenzie"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "83c07594-5e3a-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/83c07594-5e3a-11ea-b0ab-339c2307bcd4",
        title: {
          title: "IMF sets aside $50bn for coronavirus-hit countries"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T17:37:54Z",
          lastPublishDateTime: "2020-03-04T20:49:17Z"
        },
        location: {
          uri: "https://www.ft.com/content/83c07594-5e3a-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...necessary but has not acted yet. Ms Georgieva said she was comforted by the co-ordination among central banks.  “They are..."
        },
        editorial: {
          subheading:
            "Fund’s managing director warns outbreak could force multilateral lender to cut growth forecasts",
          byline: "James Politi in Washington"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "1c9e3db8-5e46-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/1c9e3db8-5e46-11ea-8033-fa40a0d65a98",
        title: {
          title:
            "Andrew Bailey insists Bank of England will move quickly on coronavirus"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T19:22:57Z",
          lastPublishDateTime: "2020-03-04T19:22:57Z"
        },
        location: {
          uri: "https://www.ft.com/content/1c9e3db8-5e46-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "..., concessionary funding for commercial banks and guidance to markets on future interest rates. But he said he differed from..."
        },
        editorial: {
          subheading:
            "Incoming governor forced to defend record at City watchdog in front of MPs",
          byline: "Delphine Strauss and Chris Giles in London"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "2696ef1c-5e42-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/2696ef1c-5e42-11ea-8033-fa40a0d65a98",
        title: {
          title: "Facebook seeks to coin new Libra"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T18:20:17Z",
          lastPublishDateTime: "2020-03-04T18:20:17Z"
        },
        location: {
          uri: "https://www.ft.com/content/2696ef1c-5e42-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...opprobrium of politicians, regulators and central banks last year with its plans for a new digital currency..."
        },
        editorial: {
          subheading:
            "Tech’s mounting coronavirus casualties, Improbable losses, HTC Exodus 5G Hub",
          byline: "Chris Nuttall in London"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "4cc8cba7-b48b-4237-b019-f040affb4c7c",
        apiUrl:
          "https://api.ft.com/content/4cc8cba7-b48b-4237-b019-f040affb4c7c",
        title: {
          title: "Rate cuts/profits: no silver bullets"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T17:54:00Z",
          lastPublishDateTime: "2020-03-04T17:54:00Z"
        },
        location: {
          uri: "https://www.ft.com/content/4cc8cba7-b48b-4237-b019-f040affb4c7c"
        },
        summary: {
          excerpt:
            "Central banks in the US, Australia and Canada have been cutting interest rates. Stocks have wilted for the most part..."
        },
        editorial: {
          subheading:
            "Corporate finances damaged by the coronavirus cannot be revived by cheaper money"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "a8b8d09e-5e03-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/a8b8d09e-5e03-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Failure to raise equity puts Intu the breach, dear lenders"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T14:41:36Z",
          lastPublishDateTime: "2020-03-04T17:18:49Z"
        },
        location: {
          uri: "https://www.ft.com/content/a8b8d09e-5e03-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "Intu, or intu as it likes to be known, has failed to raise the £1.5bn in equity it needed to get its banks off its back..."
        },
        editorial: {
          subheading:
            "Intu speeds towards covenant limits; Bailey drops catches; Sirius clodhoppers",
          byline: "Kate Burgess"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "1ec44580-5e17-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/1ec44580-5e17-11ea-8033-fa40a0d65a98",
        title: {
          title: "What bond markets tell us about Christine Lagarde’s next move"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T17:08:54Z",
          lastPublishDateTime: "2020-03-04T17:08:54Z"
        },
        location: {
          uri: "https://www.ft.com/content/1ec44580-5e17-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...reportedly considering a further round of cheap loans to banks, targeted at those that fund small and..."
        },
        editorial: {
          subheading:
            "The European Central Bank does not enjoy the same room for manoeuvre as the Fed",
          byline: "Tommy Stubbington"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "a0526ce6-5e29-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/a0526ce6-5e29-11ea-8033-fa40a0d65a98",
        title: {
          title: "Bank of Canada cuts interest rates in face of coronavirus"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T15:28:58Z",
          lastPublishDateTime: "2020-03-04T15:28:58Z"
        },
        location: {
          uri: "https://www.ft.com/content/a0526ce6-5e29-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...Federal Reserve to ease monetary policy in the wake of the coronavirus outbreak. Central banks and finance..."
        },
        editorial: {
          subheading:
            "The cut matches the Fed’s emergency rate cut to counter the fallout from the outbreak",
          byline: "Matthew Rocco in New York"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "71ba52d8-5daa-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/71ba52d8-5daa-11ea-b0ab-339c2307bcd4",
        title: {
          title:
            "Record wave of US refinancing forecast after Federal Reserve’s rate cut"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T13:14:53Z",
          lastPublishDateTime: "2020-03-04T13:14:53Z"
        },
        location: {
          uri: "https://www.ft.com/content/71ba52d8-5daa-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...mortgage loans are ultimately packaged into tradeable securities, banks’ balance sheet capacity does not constrain..."
        },
        editorial: {
          subheading:
            "Accompanying dive in long-term bond yields expected to push mortgage rates lower",
          byline: "Robert Armstrong in New York"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "d739ef14-5da6-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/d739ef14-5da6-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Banks test disaster recovery sites on coronavirus fears"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T12:35:30Z",
          lastPublishDateTime: "2020-03-04T12:35:30Z"
        },
        location: {
          uri: "https://www.ft.com/content/d739ef14-5da6-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "Banks are sending hundreds of staff to their UK and US disaster recovery sites, installing big screens in traders..."
        },
        editorial: {
          subheading:
            "What happens when at-home trading competes for bandwidth with Netflix?",
          byline: "Laura Noonan in New York and Stephen Morris in London"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "26a63d20-5d6e-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/26a63d20-5d6e-11ea-b0ab-339c2307bcd4",
        title: {
          title: "FirstFT: Today’s top stories"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-03T23:05:12Z",
          lastPublishDateTime: "2020-03-04T12:27:54Z"
        },
        location: {
          uri: "https://www.ft.com/content/26a63d20-5d6e-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...has overturned a ban on banks dealing with cryptocurrencies , providing vital relief to an industry that had been pushed..."
        },
        editorial: {
          subheading: "Your daily briefing on the news",
          byline: "Gordon Smith, Eli Meixler and Emily Goldberg"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "d2010056-3f5d-461d-bc22-5586a7ec8655",
        apiUrl:
          "https://api.ft.com/content/d2010056-3f5d-461d-bc22-5586a7ec8655",
        title: {
          title: "Markets now, Wednesday 4th March 2020"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T12:23:55Z",
          lastPublishDateTime: "2020-03-04T12:23:55Z"
        },
        location: {
          uri: "https://www.ft.com/content/d2010056-3f5d-461d-bc22-5586a7ec8655"
        },
        summary: {
          excerpt:
            "...are not enough as markets are questioning the omnipotence of central banks, even bolder measures become likely. For the..."
        },
        editorial: {
          subheading:
            "Featuring Rio Tinto, Anglo American, Sirius Minerals, Intu Properties, Nanoco, DS Smith and sandwiches.",
          byline: "Bryce Elder"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "f08b593c-5db3-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/f08b593c-5db3-11ea-b0ab-339c2307bcd4",
        title: {
          title:
            "EU risks a green deal backlash; call for common standards; a ‘carbon-positive’ challenge"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T12:07:07Z",
          lastPublishDateTime: "2020-03-04T12:07:07Z"
        },
        location: {
          uri: "https://www.ft.com/content/f08b593c-5db3-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...most ambitious agenda are the most wealthy,” he said.  All this has significant implications for banks and financial..."
        },
        editorial: {
          subheading:
            "Your guide to the investment and business revolution you can’t afford to ignore ",
          byline:
            "Gillian Tett, Patrick Temple-West, Billy Nauman and Andrew Edgecliffe-Johnson"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "59422788-5d3e-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/59422788-5d3e-11ea-b0ab-339c2307bcd4",
        title: {
          title:
            "Ditch the kebab — there’s still time for a proper Turkish lunch"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T12:00:27Z",
          lastPublishDateTime: "2020-03-04T12:00:27Z"
        },
        location: {
          uri: "https://www.ft.com/content/59422788-5d3e-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            ".... But people here are serious about lunch. Banks close for up to an hour. Office phones go unanswered. It is not quite..."
        },
        editorial: {
          subheading:
            "‘Lokantas’ and bowtied waiters keep currency crises and shopping mall modernity at bay",
          byline: "Laura Pitel"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "5b3127c4-5e01-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/5b3127c4-5e01-11ea-b0ab-339c2307bcd4",
        title: {
          title:
            "US Fed’s coronavirus rate cut is first move in a dance with markets"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T11:42:41Z",
          lastPublishDateTime: "2020-03-04T11:42:41Z"
        },
        location: {
          uri: "https://www.ft.com/content/5b3127c4-5e01-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "When the Fed and markets move together it is difficult to figure out who is watching whom — former Fed chair Ben..."
        },
        editorial: {
          subheading:
            "Investors expect more action but policymakers say fiscal policy should take the strain",
          byline:
            "Brendan Greeley in Washington, Colby Smith and Jennifer Ablan in New York"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "c2f37f02-5df1-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/c2f37f02-5df1-11ea-b0ab-339c2307bcd4",
        title: {
          title:
            "India’s top court overturns ban on banks dealing in cryptocurrencies"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T10:12:23Z",
          lastPublishDateTime: "2020-03-04T10:12:23Z"
        },
        location: {
          uri: "https://www.ft.com/content/c2f37f02-5df1-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "India’s top court has overturned a ban on banks dealing with cryptocurrencies, providing vital relief to an industry..."
        },
        editorial: {
          subheading:
            "Decision paves way for a trading revival but nascent industry still faces obstacles",
          byline: "Benjamin Parkin in Mumbai"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "473fe380-5dee-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/473fe380-5dee-11ea-8033-fa40a0d65a98",
        title: {
          title: "China’s high-tech champions get preferential treatment"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T09:01:18Z",
          lastPublishDateTime: "2020-03-04T09:01:18Z"
        },
        location: {
          uri: "https://www.ft.com/content/473fe380-5dee-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...Tokyo, from the sleek head offices of Japan’s big banks in Marunouchi to those of SoftBank, close to Tokyo Bay.  Alibaba..."
        },
        editorial: {
          subheading:
            "Tech Scroll Asia, your guide to the billions made and lost in Asia tech",
          byline: "James Kynge and Mercedes Ruehl"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "1451a570-40a7-410c-8f30-483a622a541e",
        apiUrl:
          "https://api.ft.com/content/1451a570-40a7-410c-8f30-483a622a541e",
        title: {
          title: "Further reading"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T07:47:14Z",
          lastPublishDateTime: "2020-03-04T07:47:14Z"
        },
        location: {
          uri: "https://www.ft.com/content/1451a570-40a7-410c-8f30-483a622a541e"
        },
        summary: {
          excerpt:
            ".... — French seek solace in Camus.— Central banks and coronavirus. — Confidential Tesla and SpaceX documents leaked following..."
        },
        editorial: {
          subheading: "Super Tuesday; Albert Camus; Jack Welch; and more",
          byline: "Claire Jones"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "ff7094e0-5dcb-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/ff7094e0-5dcb-11ea-b0ab-339c2307bcd4",
        title: {
          title: "EU begins arduous path to net zero climate emissions"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T07:17:21Z",
          lastPublishDateTime: "2020-03-04T07:17:21Z"
        },
        location: {
          uri: "https://www.ft.com/content/ff7094e0-5dcb-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...banks have talked tough — and in the case of the Federal Reserve, taken radical rate cut action — Christine Lagarde is..."
        },
        editorial: {
          subheading:
            "A leaked version of the proposal contains some eye-catching elements that are likely to get on the wrong side of member states",
          byline: "Mehreen Khan in Brussels"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "6e709c20-5dca-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/6e709c20-5dca-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Elliott and Bank of East Asia hit pause in battle for control"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T05:18:44Z",
          lastPublishDateTime: "2020-03-04T05:18:44Z"
        },
        location: {
          uri: "https://www.ft.com/content/6e709c20-5dca-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...investments.” BEA is one of the largest foreign banks in mainland China and has an expansive branch network in the country..."
        },
        editorial: {
          subheading:
            "Strategic review could prompt sale of parts of Hong Kong’s last big family-run bank",
          byline: "Don Weinland in Beijing"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "27cf0690-5c9d-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/27cf0690-5c9d-11ea-b0ab-339c2307bcd4",
        title: {
          title: "The seeds of the next debt crisis"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T05:04:37Z",
          lastPublishDateTime: "2020-03-04T05:04:37Z"
        },
        location: {
          uri: "https://www.ft.com/content/27cf0690-5c9d-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "..., chief economist of TS Lombard, if US banks now tighten credit while lending has become less profitable. This..."
        },
        editorial: {
          subheading:
            "With debt levels already at a record high, coronavirus raises the risk of a credit crunch in a world of low interest rates",
          byline: "John Plender in London"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "6d75fd96-5d71-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/6d75fd96-5d71-11ea-b0ab-339c2307bcd4",
        title: {
          title:
            "Coronavirus poses big test of capitalism’s stakeholder conversion"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T05:00:33Z",
          lastPublishDateTime: "2020-03-04T05:00:33Z"
        },
        location: {
          uri: "https://www.ft.com/content/6d75fd96-5d71-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "..., capitalism’s recent conversion faces its biggest test. Central banks have  moved quickly to cushion the economic..."
        },
        editorial: {
          subheading:
            "Outbreak will show whether there is substance behind pledges to manage for the long term",
          byline: "Andrew Edgecliffe-Johnson"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "aca995e0-5ca9-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/aca995e0-5ca9-11ea-8033-fa40a0d65a98",
        title: {
          title: "Letter: Bank regulators don’t even recognise goodwill"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T05:00:31Z",
          lastPublishDateTime: "2020-03-04T05:00:31Z"
        },
        location: {
          uri: "https://www.ft.com/content/aca995e0-5ca9-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...European banks is below 1, it’s because the market assigns a fair value to the banks’ net assets that is lower than..."
        },
        editorial: {
          subheading: "From Amir Amel-Zadeh, University of Oxford, UK"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "ac4082ee-5ca9-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/ac4082ee-5ca9-11ea-8033-fa40a0d65a98",
        title: {
          title:
            "Letter: Discount to stated book value reflects uncertainty over long-term returns"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T05:00:30Z",
          lastPublishDateTime: "2020-03-04T05:00:30Z"
        },
        location: {
          uri: "https://www.ft.com/content/ac4082ee-5ca9-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...concerns: potential uncertainty over whether banks’ balance sheet assets are in fact realisable in all circumstances at..."
        },
        editorial: {
          subheading: "From Michael Lever, London, UK"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "e43087e0-5dac-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/e43087e0-5dac-11ea-b0ab-339c2307bcd4",
        title: {
          title:
            "IPOs postponed and debt offerings shelved. Coronavirus-induced volatility is gumming up critical sources of capital"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T05:00:28Z",
          lastPublishDateTime: "2020-03-04T05:00:28Z"
        },
        location: {
          uri: "https://www.ft.com/content/e43087e0-5dac-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...— as well as the banks that draw billions of dollars in fees a year helping underwrite initial public offerings, corporate..."
        },
        editorial: {
          subheading: "Welcome to Due Diligence, the FT’s daily deals briefing"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "bff92220-5d79-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/bff92220-5d79-11ea-8033-fa40a0d65a98",
        title: {
          title: "Fed rate cut piles pressure on Lagarde to take action"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T05:00:27Z",
          lastPublishDateTime: "2020-03-04T05:00:27Z"
        },
        location: {
          uri: "https://www.ft.com/content/bff92220-5d79-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...rate cut on Tuesday  piled pressure on other central banks to take action, too — none more so than the ECB, whose rate..."
        },
        editorial: {
          subheading:
            "ECB constrained by record low interest rates as it weighs response to coronavirus",
          byline: "Martin Arnold in Frankfurt"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "7fd94f91-9042-4fa1-8418-4020357d946a",
        apiUrl:
          "https://api.ft.com/content/7fd94f91-9042-4fa1-8418-4020357d946a",
        title: {
          title: "Nationwide launches cash Isa prize draw"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T05:00:11Z",
          lastPublishDateTime: "2020-03-04T05:00:11Z"
        },
        location: {
          uri: "https://www.ft.com/content/7fd94f91-9042-4fa1-8418-4020357d946a"
        },
        summary: {
          excerpt:
            "Nationwide will offer cash Isa savers the chance of winning up to £20,000 as falling interest rates prompt more banks..."
        },
        editorial: {
          subheading:
            "As interest rates fall, more savings banks rely on the lure of cash prizes",
          byline: "Katharine Gemmell"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "ff40bc98-5dc1-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/ff40bc98-5dc1-11ea-b0ab-339c2307bcd4",
        title: {
          title: "New data paint grim picture of coronavirus fallout"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T04:37:07Z",
          lastPublishDateTime: "2020-03-04T04:37:07Z"
        },
        location: {
          uri: "https://www.ft.com/content/ff40bc98-5dc1-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...Federal Reserve  made an emergency cut in interest rates on Tuesday and will heighten pressure on Asian central banks to..."
        },
        editorial: {
          subheading:
            "Mainland China and Hong Kong PMI readings fall to record low",
          byline: "Robin Harding in Tokyo"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "0bd9d2ea-5c15-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/0bd9d2ea-5c15-11ea-8033-fa40a0d65a98",
        title: {
          title: "Heavy flows into ESG funds raise questions over ratings"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T04:00:35Z",
          lastPublishDateTime: "2020-03-04T04:00:35Z"
        },
        location: {
          uri: "https://www.ft.com/content/0bd9d2ea-5c15-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...’ rankings for environmental, social and governance criteria, and some banks are even offering better borrowing terms to..."
        },
        editorial: {
          subheading:
            "Providers under spotlight on the integrity of their scoring systems",
          byline: "Billy Nauman in New York"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "33fe6698-58a3-11ea-a528-dd0f971febbc",
        apiUrl:
          "https://api.ft.com/content/33fe6698-58a3-11ea-a528-dd0f971febbc",
        title: {
          title: "UK businesses prepare to face off with shareholders over pay"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T04:00:33Z",
          lastPublishDateTime: "2020-03-04T04:00:33Z"
        },
        location: {
          uri: "https://www.ft.com/content/33fe6698-58a3-11ea-a528-dd0f971febbc"
        },
        summary: {
          excerpt:
            "...season if [boards] are listening.” Pensions After forcing banks such as Standard Chartered and Lloyds to slash pensions..."
        },
        editorial: {
          subheading:
            "High pension awards and bonus increases set scene for fraught annual meetings season",
          byline: "Attracta Mooney, Investment Correspondent"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "d593c66c-5d6a-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/d593c66c-5d6a-11ea-8033-fa40a0d65a98",
        title: {
          title:
            "Hedge funds Caxton and Kirkoswald profit from bets on lower rates"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T04:00:29Z",
          lastPublishDateTime: "2020-03-04T04:00:29Z"
        },
        location: {
          uri: "https://www.ft.com/content/d593c66c-5d6a-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...banks to combat the economic damage caused by the coronavirus, and as investors have sought havens from the stock market..."
        },
        editorial: {
          subheading:
            "Soaring government bonds deliver bumper returns in angst-ridden markets",
          byline: "Laurence Fletcher in London"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "4ab1c93c-5d7d-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/4ab1c93c-5d7d-11ea-8033-fa40a0d65a98",
        title: {
          title:
            "Coronavirus raises the risk of real trouble in corporate bonds"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T04:00:27Z",
          lastPublishDateTime: "2020-03-04T04:00:27Z"
        },
        location: {
          uri: "https://www.ft.com/content/4ab1c93c-5d7d-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "..., this is not a financial  sudden stop, in which central banks can intervene to ease fears over counterparty risk, fix..."
        },
        editorial: {
          subheading:
            "Even the Fed’s rate cut cannot prevent economic ‘sudden stops’ that will hurt business",
          byline: "Mohamed El-Erian"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "09479b66-5486-11ea-8841-482eed0038b1",
        apiUrl:
          "https://api.ft.com/content/09479b66-5486-11ea-8841-482eed0038b1",
        title: {
          title: "Coronavirus exposes cracks in south-east Asia economies"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T02:15:26Z",
          lastPublishDateTime: "2020-03-04T02:15:26Z"
        },
        location: {
          uri: "https://www.ft.com/content/09479b66-5486-11ea-8841-482eed0038b1"
        },
        summary: {
          excerpt:
            "...spreads. Worries about the impact of coronavirus prompted some central banks to cut rates to cushion any lingering effects..."
        },
        editorial: {
          subheading:
            "Crisis highlights their heavy dependence on China for tourists and trade",
          byline: "John Reed in Bangkok and Stefania Palma in Singapore"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "ad632942-5ca9-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/ad632942-5ca9-11ea-8033-fa40a0d65a98",
        title: {
          title: "Letter: EU banks’ efforts in the US were always doomed"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-04T00:01:11Z",
          lastPublishDateTime: "2020-03-04T00:01:11Z"
        },
        location: {
          uri: "https://www.ft.com/content/ad632942-5ca9-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "Your Big Read article “ A retreat from Wall Street” (March 3) has captured European investment banks’ failure in US..."
        },
        editorial: {
          subheading:
            "From Pete Hahn, Retired Dean, The London Institute of Banking & Finance"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "eed05754-5d00-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/eed05754-5d00-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Markets tumble as Fed rate cut fails to ease fears"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-03T04:29:52Z",
          lastPublishDateTime: "2020-03-03T21:47:51Z"
        },
        location: {
          uri: "https://www.ft.com/content/eed05754-5d00-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "..., senior vice-president in Moody’s Financial Institutions Group, said the rate cut “is a credit negative for US banks..."
        },
        editorial: {
          subheading:
            "Benchmark 10-year Treasury yield falls below 1% for first time while stocks drop nearly 3%",
          byline:
            "Richard Henderson and Colby Smith in New York and Philip Georgiadis in London"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "bfb7c6ba-5d81-11ea-b0ab-339c2307bcd4",
        apiUrl:
          "https://api.ft.com/content/bfb7c6ba-5d81-11ea-b0ab-339c2307bcd4",
        title: {
          title: "Fed fails to calm the churning market waters"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-03T21:19:00Z",
          lastPublishDateTime: "2020-03-03T21:19:00Z"
        },
        location: {
          uri: "https://www.ft.com/content/bfb7c6ba-5d81-11ea-b0ab-339c2307bcd4"
        },
        summary: {
          excerpt:
            "...and Malaysia earlier on Tuesday.  Central banks with room to ease are moving to head off greater financial market..."
        },
        editorial: {
          subheading:
            "Mike Mackenzie’s daily analysis of what’s moving global markets",
          byline: "Michael Mackenzie"
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "17dd0122-5d8a-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/17dd0122-5d8a-11ea-8033-fa40a0d65a98",
        title: {
          title:
            "Zelensky shakes up Ukraine government and proposes new prime minister"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-03T20:54:27Z",
          lastPublishDateTime: "2020-03-03T20:54:27Z"
        },
        location: {
          uri: "https://www.ft.com/content/17dd0122-5d8a-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...owners of banks closed or nationalised as part of a clean-up of the sector from reclaiming the businesses or seeking..."
        },
        editorial: {
          subheading:
            "President frustrated by current rate of reform but change comes at tricky time for the nation",
          byline: "Roman Olearchyk in Kyiv"
        }
      },
      {
        aspectSet: "video",
        modelVersion: "1",
        id: "051f71b9-82b5-45ed-8c28-3ce3b28a4e6c",
        apiUrl:
          "https://api.ft.com/content/051f71b9-82b5-45ed-8c28-3ce3b28a4e6c",
        title: {
          title: "Coronavirus contingency planning"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-03T20:28:23Z",
          lastPublishDateTime: "2020-03-03T20:28:23Z"
        },
        location: {
          uri: "https://www.ft.com/content/051f71b9-82b5-45ed-8c28-3ce3b28a4e6c"
        },
        editorial: {
          byline:
            "The Financial Times banking team discusses the biggest banking stories of the week, bringing you global insight and commentary on the top issues concerning this sector. To take part in the show or to comment please email audio@ft.com"
        },
        summary: {
          excerpt:
            "Matthew Vincent and guests discuss European banks' readiness to handle a coronavirus epidemic, whether Europe’s..."
        }
      },
      {
        aspectSet: "article",
        modelVersion: "1",
        id: "f46c6d0c-5d63-11ea-8033-fa40a0d65a98",
        apiUrl:
          "https://api.ft.com/content/f46c6d0c-5d63-11ea-8033-fa40a0d65a98",
        title: {
          title: "Fed rate cut is no cure-all for coronavirus woes"
        },
        lifecycle: {
          initialPublishDateTime: "2020-03-03T20:00:26Z",
          lastPublishDateTime: "2020-03-03T20:00:26Z"
        },
        location: {
          uri: "https://www.ft.com/content/f46c6d0c-5d63-11ea-8033-fa40a0d65a98"
        },
        summary: {
          excerpt:
            "...thinking of cutting rates in remarks last week. Stocks fell further as Mr Powell explained the central banks’..."
        },
        editorial: {
          subheading:
            "Other central banks will now have to decide whether to follow suit",
          byline: "The editorial board"
        }
      }
    ]
  }
];

module.exports = resultMock;
