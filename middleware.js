const express = require("express");
const formidableMiddleware = require("express-formidable");

const initApp = app => {
  app.use(express.static("public"));
  app.use(formidableMiddleware());
};

module.exports = initApp;