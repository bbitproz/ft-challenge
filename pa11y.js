const pa11y = require("pa11y");

const DEV_ENV = "http://localhost:3002";

pa11y(DEV_ENV).then(results => {
  console.log("Home Page", results);
});

pa11y(`${DEV_ENV}/article/b55857dc-6198-11ea-b3f3-fe4680ea68b5`).then(results => {
  console.log("Article", results);
});
